INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_P_BLOCKS p_blocks)

FIND_PATH(
    P_BLOCKS_INCLUDE_DIRS
    NAMES p_blocks/api.h
    HINTS $ENV{P_BLOCKS_DIR}/include
        ${PC_P_BLOCKS_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    P_BLOCKS_LIBRARIES
    NAMES gnuradio-p_blocks
    HINTS $ENV{P_BLOCKS_DIR}/lib
        ${PC_P_BLOCKS_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
)

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(P_BLOCKS DEFAULT_MSG P_BLOCKS_LIBRARIES P_BLOCKS_INCLUDE_DIRS)
MARK_AS_ADVANCED(P_BLOCKS_LIBRARIES P_BLOCKS_INCLUDE_DIRS)

