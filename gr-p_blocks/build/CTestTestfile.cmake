# CMake generated Testfile for 
# Source directory: /home/martin/radio/project/gr-p_blocks
# Build directory: /home/martin/radio/project/gr-p_blocks/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(include/p_blocks)
subdirs(lib)
subdirs(swig)
subdirs(python)
subdirs(grc)
subdirs(apps)
subdirs(docs)
