#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Copyright 2019 <+YOU OR YOUR COMPANY+>.
# 
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
# 
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this software; see the file COPYING.  If not, write to
# the Free Software Foundation, Inc., 51 Franklin Street,
# Boston, MA 02110-1301, USA.
# 

import numpy
from gnuradio import gr
import cv2
import sys

class Manchester(object):
    """
    G. E. Thomas: 0 = 01, 1 = 10
    ISO 802.4: 0 = 10, 1 = 01
    """
    _bit_symbol_map = {
        # bit: symbol
        '0': '01',
        '1': '10',
        'invert': {
            # bit: symbol
            '0': '10',
            '1': '01'},
        'differential': {
            # (init_level, bit): symbol
            ('1', '0'): '01',
            ('0', '0'): '10',
            ('0', '1'): '11',
            ('1', '1'): '00'
        }
    }

    def __init__(self, differential=False, invert=False):
        self._invert = invert
        self._differential = differential
        self._init_level = '0'

    def decode(self, symbols):
        bits = ''
        while len(symbols):
            symbol = symbols[0:2]
            symbols = symbols[2:]

            if self._differential:
                for ib, s in self._bit_symbol_map['differential'].items():
                    if symbol == s:
                        bits += ib[1]
                continue

            if self._invert:
                for b, s in self._bit_symbol_map['invert'].items():
                    if symbol == s:
                        bits += b
                continue

            for b, s in self._bit_symbol_map.items():
                if symbol == s:
                    bits += b

        return bits

class parse_bytes_py_b(gr.sync_block):
    data = numpy.array([])
    state = "looking"
    codec = Manchester()

    def __init__(self):
        gr.sync_block.__init__(self,
            name="parse_bytes_py_b",
            in_sig=[numpy.byte],
            out_sig=[numpy.byte])

    def parse_array(self, arr):
        # Start sequence to look for
        start_seq = numpy.array([1,0,1,0,1,0,1,0], dtype=numpy.uint8)
        arr = arr.astype(numpy.uint8)
        #print("arr", arr)

        # Using OpenCV to find the index of the start sequence
        res = cv2.matchTemplate(arr, start_seq, cv2.TM_CCOEFF_NORMED)

        # If start sequence is found
        if 1 in res:
            #print("arr", arr)
            start_ind = numpy.argmax(res)

            encoded_signal = arr[start_ind:(start_ind+64)]
            #print("shape", full_signal.shape)

            #print("full signal", full_signal)

            #print("res:", res)

            str_encoded_signal = numpy.array2string(encoded_signal, max_line_width=66, separator="")[1:-1]

            #print("str_signal", str_signal)
            decoded_signal = self.codec.decode(str_encoded_signal)

            st = int(decoded_signal[7:23],2)

            #print("dec", dec_nr)

            temp = -45 + 175 * (st / (2**16.0-1))

            return temp


    def work(self, input_items, output_items):
        in0 = input_items[0]
        out = output_items[0]
        # <+signal processing here+>
        
        if self.state == "looking":
            #print(self.state)
            if 1 in in0:
                self.state = "receiving"
                ind = numpy.nonzero(in0)[0][0]

                self.data = numpy.hstack((self.data,in0[ind:]))

        elif self.state == "receiving":
            #print(self.state)
            if len(self.data) < 128:
                self.data = numpy.hstack((self.data,in0))

            elif len(self.data) >= 128:
                #print(self.data)
                temperature = self.parse_array(self.data)

                print temperature

                self.data = numpy.array([])
                self.state = "looking"

        out[:] = in0
        return len(output_items[0])

